/* Praecipio Consulting
 * christopher pepe <pepeca@praecipio.com>
 * Copyright (c) 2011
 * All rights reserved.
 
 Path to listener: com.praecipio.jira.plugins.MentionListener
 
 Set up logging/debugging in your log4j.properties file
 log4j.logger.com.praecipio.jira.plugins = DEBUG, console, filelog
 log4j.additivity.com.praecipio.jira.plugins = false
 
 */

package com.praecipio.jira.plugins;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.ManagerFactory;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.event.issue.AbstractIssueEventListener;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.issue.IssueEventListener;
import com.atlassian.jira.event.user.UserEvent;
import com.atlassian.jira.event.user.UserEventListener;
import com.atlassian.jira.mail.IssueMailQueueItemFactory;
import com.atlassian.jira.mail.UserMailQueueItem;
import com.atlassian.jira.notification.NotificationRecipient;
import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.scheme.SchemeEntity;
import com.atlassian.jira.notification.NotificationRecipient;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.mail.queue.MailQueueItem;
import com.atlassian.jira.issue.watchers.WatcherManager;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import java.lang.Object;
import java.util.*;

/**
 * A listener for emailing notifications.
 * <p>
 * At the moment when this listener is activated it will email the reporter, assignee and any watchers.
 * <p>
 * The content of the emails is governed by Velocity templates (in /templates) and the emails are sent
 * out by the MailSender.
 * <p>
 * Parameters:
 * email.from        - who the emails should be sent from
 * email.server        - the SMTP server to send email through
 * email.session    - the JNDI location of a MailSession to use when sending email
 * subject.prefix    - (optional) any prefix for the subject, like "[FooBar]" -> Subject: [FooBar] BazBat
 *
 * @see DebugMailListener
 */
public class MentionListener extends AbstractIssueEventListener implements IssueEventListener
{
    private static final Logger log = Logger.getLogger(MentionListener.class);

    private final NotificationSchemeManager notificationSchemeManager;
    private final IssueMailQueueItemFactory issueMailQueueItemFactory;
    private final ApplicationProperties applicationProperties;
    private final WatcherManager watcherManager;
	private UserUtil userUtil = ComponentManager.getInstance().getUserUtil();

    public MentionListener(NotificationSchemeManager notificationSchemeManager, IssueMailQueueItemFactory issueMailQueueItemFactory, ApplicationProperties applicationProperties, WatcherManager watcherManager)
    {
        this.notificationSchemeManager = notificationSchemeManager;
        this.issueMailQueueItemFactory = issueMailQueueItemFactory;
        this.applicationProperties = applicationProperties;
        this.watcherManager = watcherManager;
    }

    public void init(Map params)
    {
    }

    public String[] getAcceptedParams()
    {
        return new String[0];
    }

    /**
     * Retrieve the assocaited notification scheme and create the mail items for notification of the specified event.
     *
     * @param event
     */
    protected void sendNotification(IssueEvent event, Set mentions)
    {
        if (event.isSendMail())
        {
            GenericValue projectGV = event.getIssue().getProject();
            GenericValue notificationScheme = notificationSchemeManager.getNotificationSchemeForProject(projectGV);

            if (notificationScheme != null)
                createMailItems(notificationScheme, event, mentions);
        }
    }

    /**
     * Add mail items to the mail queue for the event and the associated notification types as defined in the notification
     * scheme.
     * <p>
     * Only the first email encountered for a user will be sent in the case where a user is included in multiple notification
     * types.
     *
     * @param notificationScheme    used to determine the recipients of the notification of the specified event
     * @param event                 the cause of the notification
     */
    protected void createMailItems(GenericValue notificationScheme, IssueEvent event, Set mentions)
    {
        try
        {
            // Retrieve the entities related to this event type from the notification schemee
            Collection notificationSchemeEntities = notificationSchemeManager.getEntities(notificationScheme, event.getEventTypeId());
            Long templateId = ComponentManager.getInstance().getTemplateManager().getTemplate( event.getEventTypeId() ).getId(); 
            /*
            // Retain a list of ALL recipients.
            // This set ensures that a user will only recieve one email regarding this issue event
            Set allRecipients = new HashSet();

            for (Iterator iterator = notificationSchemeEntities.iterator(); iterator.hasNext();)
            {
                GenericValue schemeEntity = (GenericValue) iterator.next();
                SchemeEntity notificationSchemeEntity = new SchemeEntity(schemeEntity.getLong("id"), schemeEntity.getString("type"), schemeEntity.getString("parameter"), schemeEntity.get("eventTypeId"), schemeEntity.get("templateId"), schemeEntity.getLong("scheme"));


                Long templateId = ComponentManager.getInstance().getTemplateManager().getTemplate(notificationSchemeEntity).getId();

                // The intended recipient list - a user will only recieve the first email encountered for them
                // Any further emails intended for that user in relation to this notification event will not be sent
                Collection intendedRecipients = notificationSchemeManager.getRecipients(event, notificationSchemeEntity);
                Set actualRecipients = new HashSet();

                if (intendedRecipients != null && !intendedRecipients.isEmpty())
                {
                    for (Iterator iterator1 = intendedRecipients.iterator(); iterator1.hasNext();)
                    {
                        NotificationRecipient notificationRecipient = (NotificationRecipient) iterator1.next();
                        // Check if the recipient is already included in the recipient list for another template for this event
                        if (!allRecipients.contains(notificationRecipient))
                        {
                            actualRecipients.add(notificationRecipient);
                            allRecipients.add(notificationRecipient);
                        }
                        else
                        {
                            log.debug("Multiple event (" + event.getEventTypeId() + ") notification emails intended for the recipient: " + notificationRecipient.getUser() + ". Sending first intended email only for the event.");
                        }
                    }

                    if (!actualRecipients.isEmpty())
                    {
                        MailQueueItem item = issueMailQueueItemFactory.getIssueMailQueueItem(event, templateId, mentions, notificationSchemeEntity.getType());
                        ManagerFactory.getMailQueue().addItem(item);
                    }
                }
            }*/
            if (!mentions.isEmpty())
            {
                MailQueueItem item = issueMailQueueItemFactory.getIssueMailQueueItem(event, templateId, mentions, "mention");
                ManagerFactory.getMailQueue().addItem(item);
            }
        }
        catch (GenericEntityException e)
        {
            log.error("There was an error accessing the notifications for the scheme: " + notificationScheme.getString("name") + ".", e);
        }
    }

    // IssueEventListener implementation -------------------------------------------------------------------------------
    public void issueCreated(IssueEvent event)
    {
        handleEvent(event);
    }

    public void issueAssigned(IssueEvent event)
    {
        handleEvent(event);
    }

    public void issueClosed(IssueEvent event)
    {
        handleEvent(event);
    }

    public void issueResolved(IssueEvent event)
    {
        handleEvent(event);
    }

    public void issueReopened(IssueEvent event)
    {
        handleEvent(event);
    }

    public void issueUpdated(IssueEvent event)
    {
        handleEvent(event);
    }

    public void issueCommented(IssueEvent event)
    {
        handleEvent(event);
    }

    public void issueCommentEdited(IssueEvent event)
    {
        handleEvent(event);
    }

    public void issueWorkLogged(IssueEvent event)
    {
        handleEvent(event);
    }

    public void issueWorklogUpdated(IssueEvent event)
    {
        handleEvent(event);
    }

    public void issueWorklogDeleted(IssueEvent event)
    {
        handleEvent(event);
    }

    public void issueDeleted(IssueEvent event)
    {
        handleEvent(event);
    }

    public void issueMoved(IssueEvent event)
    {
        handleEvent(event);
    }

    public void issueStarted(IssueEvent event)
    {
        handleEvent(event);
    }

    public void issueStopped(IssueEvent event)
    {
        handleEvent(event);
    }

    public void issueGenericEvent(IssueEvent event)
    {
        handleEvent(event);
    }

    public void customEvent(IssueEvent event)
    {
        handleEvent(event);
    }

    //whether administrators can delete this listener
    public boolean isInternal()
    {
        return false;
    }

    /**
     * Mail Listeners are unique.  It would be a rare case when you would want two emails sent out.
     */
    public boolean isUnique()
    {
        return true;
    }

    public String getDescription()
    {
        return "Allows managing watch list and notifications via twitter style @mentions";
    }    

    /****** mention stuff below here ******/
    private void handleEvent(IssueEvent event){
        try{
            //see if there was a comment
            String comment = event.getComment().getBody();
            //get new mentions to email, update watch list
            Set mentions = handleMentionedWatchers( comment, event );
            //send notificatons (this shouldn't really be needed since the dfl listener should do it
            //but it won't handle the new watchers)
            sendNotification(event, mentions);
        }
        catch (Exception e){
            log.debug(event.getIssue().getKey() + " does not have a comment to process: " + e.toString());
        }
    }
    //Return a list of mentioned users
    //@username mentions and adds user to the watch list
    //-@username removes the user from the watch list
    /* This function returns a list of mentioned users and add/removes users if they have a
        mention/remove request */
    private Set handleMentionedWatchers(String c, IssueEvent event){
        Set mentions = new HashSet(); //list of NotificatonRecipients
        Set removals = new HashSet(); //list of Strings "username"
        //if there are no mentions in the comment short circuit this business
        if (c.indexOf("@") < 0)
        {
            log.debug("No mentions in " + c);
            return null;
        }
        //there's at least 1 mention
        //split on spaces and look for strings starting with @
        String[] parts = c.split(" ");
        for (String s: parts){
            //looks like a mention
            if( s.startsWith("@")){
                String mention_name = s.substring(1);
                log.debug(s + " looks like a mention");
                try{
                    //try to strip chars off the end looking for a valid user. This seems
                    //kinda dangerous i.e. Users robert and rob. Typo @robwrt (should be @robert) would resovle to @rob
                    while( mention_name.length() >= 3){
                        User t_user = userUtil.getUserObject( mention_name );
                        if (t_user != null){
                            log.debug("Got valid user for " + mention_name);
                            NotificationRecipient nr_user = new NotificationRecipient(t_user);
                            mentions.add( nr_user );
                            break;
                        }
                        else{
                            mention_name = mention_name.substring(0, mention_name.length()-1);
                            log.debug("Trying to find username " + mention_name);
                            
                        }
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                    log.error(e);
                }
            }
            else if (s.startsWith("-@")){
                //looks like a remove request
                String remove_name = s.substring(2);
                log.debug(s + " looks like a remove request");
                try{
                    User t_user = userUtil.getUserObject( remove_name );
                    if (t_user != null){
                        log.debug("Found valid user to remove named " + remove_name);
                        //NotificationRecipient nr_user = new NotificationRecipient(t_user);
                        removals.add( remove_name );
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                    log.error(e);
                }
            }
        }
        setWatchers(mentions, event.getIssue());
        removeWatchers(removals, event.getIssue());
        
        //User u = ;
        log.info("Found " + Integer.toString( mentions.size() ) + " mentions and " + Integer.toString( removals.size() ) + " removals");
        //TODO: subtract out the existing watchers, assignees, and reporters (doesn't handle custom fields)
        return mentions;
    }
        
    private void setWatchers(Set<NotificationRecipient> users, Issue issue){
        //takes a Set of users to add to the watch list. If user is already on list they are silently
        //ignored since the end result is the same
        GenericValue gvIssue = issue.getGenericValue();
        for (NotificationRecipient u : users){
            User t_user = (User)u.getUser(); 
            watcherManager.isWatching(t_user, issue);
            //watcherManager.startWatching(t_user, issue);
            watcherManager.startWatching(t_user, gvIssue);
        }
    }

    private void removeWatchers(Set<String> users, Issue issue){
        //takes a Set of users to add to the watch list. If user is already on list they are silently
        //ignored since the end result is the same 
        for (String u : users){
            User t_user = (User)userUtil.getUserObject( u );
            watcherManager.isWatching(t_user, issue);
            watcherManager.stopWatching(t_user, issue);
        }
    }
}
